package br.com.oauth.gestordecontatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorDeContatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorDeContatosApplication.class, args);
	}

}
