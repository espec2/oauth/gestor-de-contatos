package br.com.oauth.gestordecontatos.service;

import br.com.oauth.gestordecontatos.model.Contato;
import br.com.oauth.gestordecontatos.repository.ContatoRepository;
import br.com.oauth.gestordecontatos.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {
    @Autowired
    ContatoRepository contatoRepository;

    public Contato incluirContato(Contato contato){
        return contatoRepository.save(contato);
    }

    public List<Contato> getByUsuarioId(Usuario usuario){
        return contatoRepository.findByUsuarioId(usuario.getId());
    }

}
