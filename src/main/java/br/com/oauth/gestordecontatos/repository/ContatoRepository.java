package br.com.oauth.gestordecontatos.repository;

import br.com.oauth.gestordecontatos.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    List<Contato> findByUsuarioId(Integer id);
}
