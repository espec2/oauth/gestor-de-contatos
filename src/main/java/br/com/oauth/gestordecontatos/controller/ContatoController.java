package br.com.oauth.gestordecontatos.controller;

import br.com.oauth.gestordecontatos.model.Contato;
import br.com.oauth.gestordecontatos.model.dto.ContatoDTO;
import br.com.oauth.gestordecontatos.model.mapper.ContatoMapper;
import br.com.oauth.gestordecontatos.security.Usuario;
import br.com.oauth.gestordecontatos.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContatoController {
    @Autowired
    ContatoService contatoService;

    @PostMapping("/contato")
    public ContatoDTO obterUsuario(@RequestBody ContatoDTO contatoDto, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = ContatoMapper.fromCreateRequest(contatoDto);
        contato.setUsuarioId(usuario.getId());
        return ContatoMapper.toCreateResponse(contatoService.incluirContato(contato));
    }

    @GetMapping("/contatos")
    public List<ContatoDTO> getByNumero(@AuthenticationPrincipal Usuario usuario) {

        return ContatoMapper.toCreateResponse(contatoService.getByUsuarioId(usuario));
    }
}
