package br.com.oauth.gestordecontatos.model.dto;

public class ContatoDTO {
    private String nome;
    private String telefone;

    public ContatoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
