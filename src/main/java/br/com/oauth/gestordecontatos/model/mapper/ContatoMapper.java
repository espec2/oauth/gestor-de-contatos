package br.com.oauth.gestordecontatos.model.mapper;

import br.com.oauth.gestordecontatos.model.Contato;
import br.com.oauth.gestordecontatos.model.dto.ContatoDTO;

import java.util.ArrayList;
import java.util.List;

public class ContatoMapper {
    public static Contato fromCreateRequest(ContatoDTO dto) {

        Contato contato = new Contato();
        contato.setNome(dto.getNome());
        contato.setTelefone(dto.getTelefone());
        return contato;
    }

    public static ContatoDTO toCreateResponse(Contato contato) {

        ContatoDTO dto = new ContatoDTO();
        dto.setNome(contato.getNome());
        dto.setTelefone(contato.getTelefone());
        return dto;
    }

    public static List<ContatoDTO> toCreateResponse(List<Contato> contatos) {
        List<ContatoDTO> dtosRetorno = new ArrayList<>();
        for (Contato contato: contatos) {
            dtosRetorno.add(ContatoMapper.toCreateResponse(contato));
        }
        return dtosRetorno;
    }
}